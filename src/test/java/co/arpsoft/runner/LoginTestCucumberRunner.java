package co.arpsoft.runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = "src/test/resources/features/login.feature",
        glue = "co.arpsoft.stepdefinition",
        publish = true,
        tags = ""

)

public class LoginTestCucumberRunner {
}
