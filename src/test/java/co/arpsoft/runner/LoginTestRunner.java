package co.arpsoft.runner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import  org.hamcrest.Matchers.*;

import static org.hamcrest.Matchers.*;

public class LoginTestRunner {

    //Ejemplo cde como traer la información y mostrarla por consola
    @Test
    public void loginVersion1(){
        String response =
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("https://reqres.in/api/login")
                .then()
                .extract()
                .asString();
        System.out.println(response);
    }

    //Ejemplo cde como traer la información y validar el statusCode
    @Test
    public void loginVersion2(){
                RestAssured
                        .given()
                        .contentType(ContentType.JSON) //                .contentType("application/json")
                        .body("{\n" +
                                "    \"email\": \"eve.holt@reqres.in\",\n" +
                                "    \"password\": \"cityslicka\"\n" +
                                "}")
                        .post("https://reqres.in/api/login")
                        .then()
                        .statusCode(200)
                        .body("token",notNullValue());
    }

    //Ejemplo cde como traer la información y validar el statusCode y además metiendole log.all para msotrar info en consola.
    @Test
    public void loginVersion3(){
        RestAssured
                .given()
                .log().all()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("https://reqres.in/api/login")
                .then()
                .log().all()
                .statusCode(200)
                .body("token",notNullValue());
    }

    //Ejemplo fallando.
    @Test
    public void loginVersion4(){
        RestAssured
                .given()
                .log().all()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("https://reqres.in/api/login")
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_OK)//                .statusCode(200)
                .body("token",notNullValue());
    }

    //Ejemplo fallando en el registro falso sin contraseña.
    @Test
    public void loginVersion5(){
        RestAssured
                .given()
                .log().all()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"peter@klaven\"\n" +
                        "}\n")
                .post("https://reqres.in/api/login")
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_BAD_REQUEST)//                .statusCode(400)
                .body("error",equalTo("Missing password"));
    }

    //Ejemplo fallando en el registro falso sin contraseña.
    @Test
    public void loginVersion6(){
        RestAssured
                .given()
                .log().all()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"peter@klaven\"\n" +
                        "}\n")
                .post("https://reqres.in/api/login")
                .then()
                .log().all()
                .statusCode(HttpStatus.SC_BAD_REQUEST)//                .statusCode(400)
                .body(containsString("Missing password"));
    }

}
