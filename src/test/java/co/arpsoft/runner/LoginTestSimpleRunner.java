package co.arpsoft.runner;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.*;

public class LoginTestSimpleRunner {


    @BeforeEach
    public void setup(){
        RestAssured.baseURI = "https://reqres.in/";
        RestAssured.basePath = "api/";

        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()); // PAra reemplazar el log.all
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .build();


    }

    //Ejemplo cde como traer la información y mostrarla por consola
    @Test
    public void loginVersion1(){
        String response =
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("login")
                .then()
                .extract()
                .asString();
        System.out.println(response);
    }

    //Ejemplo cde como traer la información y validar el statusCode
    @Test
    public void loginVersion2(){
                RestAssured
                        .given()
                        .contentType(ContentType.JSON) //                .contentType("application/json")
                        .body("{\n" +
                                "    \"email\": \"eve.holt@reqres.in\",\n" +
                                "    \"password\": \"cityslicka\"\n" +
                                "}")
                        .post("login")
                        .then()
                        .statusCode(200)
                        .body("token",notNullValue());
    }

    //Ejemplo cde como traer la información y validar el statusCode y además metiendole log.all para msotrar info en consola.
    @Test
    public void loginVersion3(){
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("login")
                .then()
                .statusCode(200)
                .body("token",notNullValue());
    }

    //Ejemplo fallando.
    @Test
    public void loginVersion4(){
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"cityslicka\"\n" +
                        "}")
                .post("login")
                .then()
                .statusCode(HttpStatus.SC_OK)//                .statusCode(200)
                .body("token",notNullValue());
    }

    //Ejemplo fallando en el registro falso sin contraseña.
    @Test
    public void loginVersion5(){
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"peter@klaven\"\n" +
                        "}\n")
                .post("login")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)//                .statusCode(400)
                .body("error",equalTo("Missing password"));
    }

    //Ejemplo fallando en el registro falso sin contraseña.
    @Test
    public void loginVersion6(){
        RestAssured
                .given()
                .contentType(ContentType.JSON) //                .contentType("application/json")
                .body("{\n" +
                        "    \"email\": \"peter@klaven\"\n" +
                        "}\n")
                .post("login")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)//                .statusCode(400)
                .body(containsString("Missing password"));
    }

}
