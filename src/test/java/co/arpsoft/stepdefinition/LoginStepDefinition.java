package co.arpsoft.stepdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;


public class LoginStepDefinition extends Reqresin{

    private RequestSpecification requestSpecification;
    private Response response;

    @Given("el suario esta en la pagina de inicion de sesion con el correo de usuario {string} y contrasena {string}")
    public void el_suario_esta_en_la_pagina_de_inicion_de_sesion_con_el_correo_de_usuario_y_contrasena(String email, String password) {
        try{
            generalSetuo();
            requestSpecification = given().body("{\n" +
                    "    \"email\": \""+ email +"\",\n" +
                    "    \"password\": \""+ password +"\"\n" +
                    "}");
        }catch (Exception e){
            //Log4j - Utilizarlo.
            Assertions.fail(e.getMessage());
        }
    }


    @When("cuando el usuario hace una peticion de inicio")
    public void cuandoElUsuarioHaceUnaPeticionDeInicio() {
        try{
            response = requestSpecification.post(RESOURCE_LOGIN);


        }catch (Exception e){
            //Log4j
            Assertions.fail(e.getMessage());
        }

    }
    @Then("el usuario debera ver un codigo de respuesta exitoso y un token de respuesta")
    public void elUsuarioDeberaVerUnCodigoDeRespuestaExitosoYUnTokenDeRespuesta() {

        try{
            response.then().statusCode(HttpStatus.SC_OK).body("token", notNullValue());

        }catch (Exception e){
            //Log4j
            Assertions.fail(e.getMessage());
        }
    }

}
